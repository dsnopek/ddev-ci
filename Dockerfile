FROM ubuntu:22.04

ENV DOCKER_VERSION 5:24.0.7-1~ubuntu.22.04~jammy
ENV DDEV_VERSION v1.22.4

RUN apt-get update && apt-get install -y \
	apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    unzip \
    sudo \
    software-properties-common \
 && rm -rf /var/lib/apt/lists/*

# Install Docker
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - \
 && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
 && apt-get update \
 # Set the docker group to a consistent value.
 && groupadd -g 5000 docker \
 && apt-get install -y docker-ce=$DOCKER_VERSION \
 && rm -rf /var/lib/apt/lists/* \
 && docker --version

# Install DDEV
RUN curl -L -o /tmp/ddev.tgz https://github.com/ddev/ddev/releases/download/${DDEV_VERSION}/ddev_linux-amd64.${DDEV_VERSION}.tar.gz \
 && mkdir /tmp/ddev \
 && tar -xvf /tmp/ddev.tgz -C /tmp/ddev \
 && mv /tmp/ddev/ddev /tmp/ddev/mkcert /usr/local/bin/ \
 && rm -rf /tmp/ddev.tgz /tmp/ddev

# Add a DDEV user.
RUN useradd -ms /bin/bash ddev \
 && usermod -a -G docker ddev \
 && echo 'ddev ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/ddev \
 && chown -R ddev:ddev /home/ddev

USER ddev

# Check that DDEV works.
RUN ddev --version

WORKDIR /home/ddev
