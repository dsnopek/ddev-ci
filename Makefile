
BUILD_TAG?=local
PUSH_TAG?=latest

build:
	docker build -t registry.gitlab.com/dsnopek/ddev-ci:$(BUILD_TAG) .

push: build
	docker tag registry.gitlab.com/dsnopek/ddev-ci:$(BUILD_TAG) registry.gitlab.com/dsnopek/ddev-ci:$(PUSH_TAG)
	docker push registry.gitlab.com/dsnopek/ddev-ci:$(PUSH_TAG)

.PHONY: build push
